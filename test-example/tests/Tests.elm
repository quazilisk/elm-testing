module Tests exposing (..)

import Test exposing (..)
import Expect
import String
import Model exposing (..)


all : Test
all =
    describe "A Test Suite"
        [ test "Addition" <|
            \() ->
                Expect.equal (3 + 7) 10
        , test "String.left" <|
            \() ->
                Expect.equal "a" (String.left 1 "abcdefg")
        , test "This test should fail" <|
            \() ->
                Expect.fail "failed as expected!" 
        ]

model : Test
model =
    describe "A Test Suite for Model"
        [ test "This should add 10" <|
            \() ->
                Expect.equal 20 (Model.updateEntry 10)
        ]
